import React, { useState } from "react";
import logo from "../images/logo.png";
import "../../node_modules/bootstrap/dist/js/bootstrap.bundle";

function Navbar() {
  const [dropdown, setDropdown] = useState(false);

  const handler = () => {
    console.log(dropdown);
    setDropdown(!dropdown);
  };
  return (
    <nav
      class="navbar navbar-light "
      style={{ width: "-webkit-fill-available" }}
    >
      <div class="container">
        <a class="navbar-brand" href="#">
          <img
            style={{ filter: "grayscale(100%)" }}
            src={logo}
            alt=""
            width="137"
            height="24"
          />
        </a>
        <ul class="nav justify-content-end">
          <li class="nav-item">
            <div class="dropdown">
              <a
                class="btn bg-transparent text-white dropdown-toggle"
                href="#"
                role="button"
                id="dropdownMenuLink"
                data-bs-toggle="dropdown"
                aria-expanded="true"
                onClick={handler}
              >
                Home
              </a>

              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li>
                  <a class="dropdown-item" href="/home1">
                    Home 1
                  </a>
                </li>
                <li>
                  <a class="dropdown-item" href="/home2">
                    Home 2
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <div class="dropdown">
              <a
                class="btn bg-transparent dropdown-toggle text-white"
                href="#"
                role="button"
                id="dropdownMenuLink"
                data-bs-toggle="dropdown"
                aria-expanded="true"
                onClick={handler}
              >
                Restaurants
              </a>

              <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                <li>
                  <a class="dropdown-item" href="/restaurant1">
                    Restaurant 1
                  </a>
                </li>
                <li>
                  <a class="dropdown-item" href="/restaurant2">
                    Restaurant 2
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link text-dark text-white" href="#">
              About Us
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="">
              FAQ
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default Navbar;
