import React from "react";
import cards from "../images/cards.png";
import "../../node_modules/bootstrap/dist/js/bootstrap.bundle";
function Footer() {
  return (
    <>
      <footer className="page-footer font-small stylish-color-dark pt-4">
        <div className="container text-center text-md-left">
          <div className="row">
            <div className="col-md-4 mx-auto">
              <h6 className="font-weight-bold text-uppercase mt-3 mb-4">
                Secure payments with
              </h6>
              <p>
                <img src={cards} alt="" />
              </p>
            </div>

            <hr className="clearfix w-100 d-md-none" />

            <div className="col-md-2 mx-auto">
              <h5 className="font-weight-bold text-uppercase mt-3 mb-4">
                About
              </h5>

              <ul className="list-unstyled">
                <li>
                  <p>About us</p>
                </li>
                <li>
                  <p>Faq</p>
                </li>
                <li>
                  <p>Contacts</p>
                </li>
                <li>
                  <p>Login</p>
                </li>
              </ul>
            </div>

            <hr className="clearfix w-100 d-md-none" />

            <div className="col-md-2 mx-auto">
              <h5 className="font-weight-bold text-uppercase mt-3 mb-4">
                Newsletter
              </h5>

              <ul className="list-unstyled">
                <li>
                  <p>Join our newletter for daily updates</p>
                </li>

                <li>
                  <form className="input-group">
                    <input
                      type="text"
                      className="form-control form-control-sm"
                      placeholder="Your email"
                      aria-label="Your email"
                      aria-describedby="basic-addon2"
                    />
                    <div className="input-group-append">
                      <button
                        className="btn btn-sm btn-outline-white my-0 bg-secondary text-light"
                        type="button"
                      >
                        Subscribe
                      </button>
                    </div>
                  </form>
                </li>
              </ul>
            </div>
          </div>
        </div>

        <hr />

        <ul className="list-unstyled list-inline text-center">
          <li className="list-inline-item">
            <a className="btn-floating btn-fb mx-1">
              <i className="fab fa-facebook-f"> </i>
            </a>
          </li>
          <li className="list-inline-item">
            <a className="btn-floating btn-tw mx-1">
              <i className="fab fa-twitter"> </i>
            </a>
          </li>
          <li className="list-inline-item">
            <a className="btn-floating btn-gplus mx-1">
              <i className="fab fa-google-plus-g"> </i>
            </a>
          </li>
          <li className="list-inline-item">
            <a className="btn-floating btn-li mx-1">
              <i className="fab fa-linkedin-in"> </i>
            </a>
          </li>
          <li className="list-inline-item">
            <a className="btn-floating btn-dribbble mx-1">
              <i className="fab fa-dribbble"> </i>
            </a>
          </li>
        </ul>
      </footer>
    </>
  );
}
export default Footer;
