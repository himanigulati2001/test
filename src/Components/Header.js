import React from "react";
import Navbar from "./Navbar";
import "./Header.css";
function Header() {
  return (
    <div>
      <div class="hero-full-screen">
        <Navbar />
        <div class="middle-content-section">
          <h1>Order Takeaway or Delivery Food</h1>
          <p>
            Ridiculus sociosqu cursus neque cursus curae ante scelerisque
            vehicula.
          </p>
        </div>

        <div
          class="bottom-content-section text-white"
          data-magellan
          data-threshold="0"
        >
          <h4>
            2650 Restaurant || 5350 People Served || 12350 Registered Users
          </h4>
        </div>
      </div>

      {/* <div
        id="main-content-section"
        data-magellan-target="main-content-section"
      ></div> */}
    </div>
  );
}
export default Header;
