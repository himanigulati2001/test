import Header from "./Components/Header";
import "./App.css";
import { Switch, Route } from "react-router-dom";
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";

import Home1 from "./Components/Home1";
import Footer from "./Components/Footer";
function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/home1">
          <Home1 />
        </Route>
        <Route path="/home2">HOME 2</Route>
        <Route path="/restaurnt1">RESTAURANT 11</Route>
        <Route path="/restaurant2">RESTAURANT 2</Route>

        <Route exact path="/">
          <Header />
          <Footer />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
